import java.util.InputMismatchException;
import java.util.Scanner;

public class ExceptionHandling {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Program Pembagian Bilangan");

        boolean validInput = false;
        do {
            //edit here
            try {
                System.out.print("Masukkan pembilang: ");
                int pembilang = scanner.nextInt(); // input pembilang

                System.out.print("Masukkan penyebut: ");
                int penyebut = scanner.nextInt(); // input penyebut

                int hasil = pembagian(pembilang, penyebut);
                System.out.println("Hasil: " + hasil); // menampilkan hasil

                validInput = true;
            } catch (InputMismatchException e) { // exeption untuk bilangan bulat
                System.out.println("Input tidak valid. Masukkan bilangan bulat.");
                scanner.nextLine(); // Membersihkan input yang tidak valid dari scanner
            } catch (ArithmeticException e) { // exeption untuk angka 0
                System.out.println(e.getMessage());
            }
        } while (!validInput);

        scanner.close();
    }

    public static int pembagian(int pembilang, int penyebut) {
        //add exception apabila penyebut bernilai 0
        if (penyebut == 0) {
            throw new ArithmeticException("Penyebut tidak boleh bernilai 0.");
        }
        return pembilang / penyebut; // menghitung pembagian
    }
}